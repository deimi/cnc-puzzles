# CNC puzzles

Puzzle CNC carvings

# Variants
- [2x2](./2x2/README.md)  
Size ~140mm x 140mm

# How to create a new puzzle
## Required tools

- Inkscape
- [Inkscape Jigsaw puzzle generator](https://github.com/Neon22/inkscape-jigsaw) extension  
Alternative: [Online puzzle generator](https://draradech.github.io/jigsaw/)
- Freecad

## Workflow
1. Inkscape -> Extensions -> Render -> Lasercut jigsaw
1. Create desired puzzle
1. Fill bounded areas  
Fill by: Visible Colors  
Threshold: 25  
Close gaps: None  
1. Fill every puzzle part with white color.  
This will create a separate object for every part.  
Note: Check the filled area. Sometimes it is necessary to realign some vector points to get a better result.
1. Save as svg file
2. Import svg into Freecad project
2. Create solids as usual
