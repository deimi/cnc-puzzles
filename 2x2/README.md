![2x2](./img/2x2_puzzle.svg)  
Size ~140mm x 140mm

# CNC files

- 2.5mm_xxx_xxx.cnc  
for 2.5mm thick wood and 1.55m flat end mill drill
